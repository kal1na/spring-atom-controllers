package com.example.demo.service;

import com.example.demo.dao.model.Human;
import com.example.demo.dao.repository.HumanRepository;
import com.example.demo.service.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HumanServiceImpl implements HumanService {

    private final HumanRepository humanRepository;

    @Autowired
    public HumanServiceImpl(HumanRepository humanRepository) {
        this.humanRepository = humanRepository;
    }

    @Override
    public Iterable<Human> list() {
        return humanRepository.findAll();
    }

    @Override
    public Human get(Long id) throws ResourceNotFoundException {
        return humanRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Human with such id wasn't found"));
    }

    @Override
    public Human create(Human human) {
        return humanRepository.save(human);
    }

    @Override
    public Human update(Human human, Long id) throws ResourceNotFoundException {
        Human result = humanRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Human with such id was not found"));
        result.setAge(human.getAge());
        result.setWeight(human.getWeight());
        result.setName(human.getName());
        humanRepository.save(result);
        return result;
    }

    @Override
    public Human delete(Long id) throws ResourceNotFoundException {
        Human result = humanRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Message with such id was not found"));
        humanRepository.delete(result);
        return result;
    }
}
