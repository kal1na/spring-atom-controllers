package com.example.demo.service;

import com.example.demo.dao.model.Message;
import com.example.demo.dao.repository.MessageRepository;
import com.example.demo.service.exception.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class MessageServiceImpl implements MessageService {

    private final MessageRepository messageRepository;

    @Autowired
    public MessageServiceImpl(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @Override
    public Iterable<Message> list() {
        return messageRepository.findAll();
    }

    @Override
    public Message get(Long id) throws ResourceNotFoundException {
        return messageRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Message with such id was not found"));
    }

    @Override
    public Message create(Message message) {
        return messageRepository.save(message);
    }

    @Override
    public Message update(Message message, Long id) throws ResourceNotFoundException {
        Message result = messageRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Message with such id was not found"));
        result.setDescription(message.getDescription());
        result.setTitle(message.getTitle());
        return result;
    }

    @Override
    public Message delete(Long id) throws ResourceNotFoundException {
        Message result = messageRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Message with such id was not found"));
        messageRepository.delete(result);
        return result;
    }
}
