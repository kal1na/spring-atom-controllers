package com.example.demo.service;

import com.example.demo.dao.model.Message;
import com.example.demo.service.exception.ResourceNotFoundException;

public interface MessageService {

    Iterable<Message> list();

    Message get(Long id) throws ResourceNotFoundException;

    Message create(Message message);

    Message update(Message message, Long id) throws ResourceNotFoundException;

    Message delete(Long id) throws ResourceNotFoundException;

}
