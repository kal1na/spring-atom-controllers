package com.example.demo.service;

import com.example.demo.dao.model.Human;
import com.example.demo.service.exception.ResourceNotFoundException;


public interface HumanService {

    Iterable<Human> list();

    Human get(Long id) throws ResourceNotFoundException;

    Human create(Human human);

    Human update(Human human, Long id) throws ResourceNotFoundException;

    Human delete(Long id) throws ResourceNotFoundException;
}
