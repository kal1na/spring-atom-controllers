package com.example.demo.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.I_AM_A_TEAPOT, reason = "U a teapot")
public class TeapotException extends Exception {
    public TeapotException(String message) {
        super(message);
    }
}
