package com.example.demo.controller;

import com.example.demo.dao.model.Human;
import com.example.demo.service.HumanService;
import com.example.demo.service.exception.ResourceNotFoundException;
import com.example.demo.service.exception.TeapotException;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/humans")
@Slf4j
public class HumanController {

    private final HumanService humanService;

    @Autowired
    public HumanController(HumanService humanService) {
        this.humanService = humanService;
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    @ResponseBody
    public Human create(@RequestBody Human human) {
        Human result = humanService.create(human);
        log.info("CREATE human {}", result);
        return result;
    }

    @GetMapping()
    @ResponseStatus(code = HttpStatus.OK)
    @ResponseBody
    public Iterable<Human> list() {
        Iterable<Human> result = humanService.list();
        log.info("GET humans: {}", result);
        return result;
    }


    @GetMapping(path = {"/{id}"})
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public Human get(@PathVariable Long id) throws ResourceNotFoundException {
        Human human = humanService.get(id);
        log.info("GET human with id = {}: {}", id, human);
        return human;
    }

    @PutMapping(path = {"/{id}"})
    @ResponseStatus(code = HttpStatus.OK)
    @ResponseBody
    public Human update(@RequestBody Human human, @PathVariable Long id) throws ResourceNotFoundException {
        Human result = humanService.update(human, id);
        log.info("UPDATE human with id = {} : {}", id, result);
        return result;
    }

    @DeleteMapping(path = {"/{id}"})
    @ResponseStatus(code = HttpStatus.OK)
    @ResponseBody
    public Human delete(@PathVariable Long id) throws ResourceNotFoundException {
        Human result = humanService.delete(id);
        log.info("DELETE human with id = {}: {}", id, result);
        return result;
    }

    // старый запрос, не изменял
    @PostMapping(params = {"ageMultiply", "weightDivision"})
    @ResponseBody
    @ResponseStatus(code = HttpStatus.CREATED)
    public Human create(@RequestBody Human human, @RequestParam Integer ageMultiply,
                        @RequestParam Integer weightDivision) {
        human.setAge(human.getAge() * ageMultiply);
        human.setWeight(human.getAge() / weightDivision);
        log.info("CREATE human with multiply age by {}, weight division by {} : {}", ageMultiply, weightDivision, human);
        return human;
    }

    // старый запрос, не изменял
    @RequestMapping(value = "/healthcheck", produces = {MediaType.TEXT_PLAIN_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<?> healthCheck(
            @RequestHeader(value = "Content-Type", defaultValue = "application/json") String contentType) {
        if (contentType.equals("application/json")) {
            HashMap<String, String> status = new HashMap<>();
            status.put("status", "UP");
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/json");
            return new ResponseEntity<>(new JSONObject(status).toString(), headers, HttpStatus.OK);
        } else if (contentType.equals("text/plain")) {
            return new ResponseEntity<>("status\tUP", HttpStatus.OK);
        }
        return new ResponseEntity<>("No valid Content-Type", HttpStatus.BAD_REQUEST);
    }

    // старый запрос, не изменял
    @GetMapping(value = "/whoiam", params = {"teapot"})
    @ResponseStatus(code = HttpStatus.OK)
    @ResponseBody
    public String whoIAm(@RequestParam Integer teapot) throws TeapotException {
        if (teapot == 1) {
            throw new TeapotException("Sorry but u really teapot");
        }
        return "I don't know ;(";
    }

    @GetMapping("/getBaseUrl")
    public ResponseEntity<String> getBaseUrl(@RequestHeader HttpHeaders headers) {
        InetSocketAddress host = headers.getHost();
        String url = "http://" + host.getHostName() + ":" + host.getPort();
        return new ResponseEntity<String>(String.format("Base URL = %s", url), HttpStatus.OK);
    }

    @GetMapping("/multiValue")
    public ResponseEntity<String> multiValue(
            @RequestHeader MultiValueMap<String, String> headers) {
        headers.forEach((key, value) -> {
            log.info(String.format(
                    "Header '%s' = %s", key, value.stream().collect(Collectors.joining("|"))));
        });

        return new ResponseEntity<String>(
                String.format("Listed %d headers", headers.size()), HttpStatus.OK);
    }
}

