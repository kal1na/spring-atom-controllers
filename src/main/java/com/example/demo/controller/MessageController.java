package com.example.demo.controller;


import com.example.demo.dao.model.Message;
import com.example.demo.service.MessageService;
import com.example.demo.service.exception.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/messages")
@RestController
@Slf4j
public class MessageController {

    private final MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public Iterable<Message> list() {
        Iterable<Message> result = messageService.list();
        log.info("GET messages: {} ", result);
        return result;
    }

    @GetMapping("/{id}")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public Message get(@PathVariable Long id) throws ResourceNotFoundException {
        Message result = messageService.get(id);
        log.info("GET message with id = {}: {}", id, result);
        return result;
    }

    @PostMapping
    @ResponseBody
    @ResponseStatus(code = HttpStatus.CREATED)
    public Message create(@RequestBody Message message) {
        Message result = messageService.create(message);
        log.info("POST message: {}", result);
        return result;
    }

    @PutMapping("/{id}")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public Message update(@RequestBody Message message, @PathVariable Long id) throws ResourceNotFoundException {
        Message result = messageService.update(message, id);
        log.info("UPDATE message with id = {}: {}", id, result);
        return result;
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    public Message delete(@PathVariable Long id) throws ResourceNotFoundException {
        Message result = messageService.delete(id);
        log.info("DELETE message: {}", result);
        return result;
    }

}
