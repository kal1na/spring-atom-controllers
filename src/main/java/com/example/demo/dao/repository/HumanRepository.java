package com.example.demo.dao.repository;

import com.example.demo.dao.model.Human;
import org.springframework.data.repository.CrudRepository;


public interface HumanRepository extends CrudRepository<Human, Long> {
}
