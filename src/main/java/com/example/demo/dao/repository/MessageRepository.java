package com.example.demo.dao.repository;

import com.example.demo.dao.model.Message;
import org.springframework.data.repository.CrudRepository;

public interface MessageRepository extends CrudRepository<Message, Long> {
}
